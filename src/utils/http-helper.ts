import axios from "axios";
const BASE_URL = "https://api.crossref.org/works";
const queryParams = "?sort=score&order=desc&rows=20&query.bibliographic=";

export const getRequest = (url: string, params?: any) => {
  url = BASE_URL + queryParams + url;
  const options = {
    url,
    method: "get",
    ...params,
  };
  return axios(options);
};
