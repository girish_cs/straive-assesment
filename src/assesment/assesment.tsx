import { useEffect, useState } from "react";
import { getRequest } from "../utils/http-helper";
import { assesData, AssesmentData } from "./assesment-helper";
import Loader from "../images/loader.gif";
import "./assesment.css";

const Assesment = () => {
  const [assesmentData, setAssesmentData] = useState<AssesmentData[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setAssesmentData(assesData);
  }, []);

  const onReset = () => {
    setAssesmentData(assesData);
  };

  const onClickHandler = async (
    title: string,
    content: string,
    paraIndex: number
  ) => {
    setLoading(true);
    const response = await getRequest(title);
    if (
      response &&
      response.data &&
      response.data.message &&
      response.data.message.items
    ) {
      const { items } = response.data.message;
      let filteredTitle = items.find(
        (i: any) =>
          i.title &&
          i.title.length &&
          i.title[0].toLowerCase() === title.toLowerCase()
      );
      if (filteredTitle) {
        const doi = filteredTitle.DOI;
        const newContent = `${content} DOI: ${doi}`;
        const newAssesmentData = assesmentData.map((a) => {
          if (a.paraIndex === paraIndex) {
            return { ...a, disableButton: true, newContent: newContent };
          }
          return a;
        });
        setAssesmentData(newAssesmentData);
      }
      setLoading(false);
    }
  };

  const renderList = () => {
    return assesmentData && assesmentData.length
      ? assesmentData.map((data) => {
          const { title, newContent, disableButton, content, paraIndex } = data;
          return (
            <div className="row my-4 mx-2" key={paraIndex}>
              <div
                className="para col-sm-10"
                id={`par${paraIndex}`}
                title={title}
              >
                {content}
                {newContent && newContent.length ? (
                  <p className="para para-underline my-2">{newContent}</p>
                ) : (
                  ""
                )}
              </div>
              <div className="crosscheck col-sm-2">
                <button
                  className={"crosscheck-button"}
                  disabled={disableButton}
                  onClick={() => onClickHandler(title, content, paraIndex)}
                >
                  {"CrossRef Check"}
                </button>
              </div>
            </div>
          );
        })
      : "";
  };

  return (
    <div className={`container ${loading ? "loading" : ""}`}>
      <div className="reset">
        <button className={"reset-button"} onClick={onReset}>
          {"Reset"}
        </button>
      </div>
      <div>
        {loading ? <img src={Loader} className="loader" alt={"loader"} /> : ""}
      </div>
      <div className="main">{renderList()}</div>
    </div>
  );
};
export default Assesment;
