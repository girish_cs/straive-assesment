export interface AssesmentData {
  title: string;
  content: string;
  paraIndex: number;
  disableButton: boolean;
  newContent: string;
}

export const assesData: AssesmentData[] = [
  {
    title:
      "Standard normal variate transformation and de-trending of near-infrared diffuse reflectance spectra",
    content:
      "Barnes RJ, Dhanoa MS, Lister SJ (1989) Standard normal variate transformation and de-trending of near-infrared diffuse reflectance spectra. Appl Spectrosc 43(5):777",
    disableButton: false,
    paraIndex: 1,
    newContent: "",
  },
  {
    title:
      "Prediction of water-holding capacity and composition of porcine meat by comparative spectroscopy",
    content:
      "Brùndum J, Munck L, Henckel P, Karlsson A, Tornberg E, Engelsen SB (2000) Prediction of water-holding capacity and composition of porcine meat by comparative spectroscopy. Meat Sci 55(2):185",
    disableButton: false,
    paraIndex: 2,
    newContent: "",
  },
  {
    title: "FT-NIR spectroscopy and wood identification",
    content:
      "Brunner M, Eugster R, Trenka E, Bergamin-Strotz L (1996) FT-NIR spectroscopy and wood identification. Holzforschung 50(2):134",
    disableButton: false,
    paraIndex: 3,
    newContent: "",
  },
  {
    title: "Wood species identification by near-infrared spectroscopy",
    content:
      "Lazarescu C, Hart F, Pirouz Z, Panagiotidis K, Mansfield SD, Barrett JD, Avramidis S (2017) Wood species identification by near-infrared spectroscopy. International Wood Products Journal 8(1):32–35",
    disableButton: false,
    paraIndex: 4,
    newContent: "",
  },
  {
    title: "Rapid spectroscopic separation of three Canadian softwoods",
    content:
      "Dawson-Andoh B, Adedipe OE (2012) Rapid spectroscopic separation of three Canadian softwoods. Wood Sci Technol 46(6):1202",
    disableButton: false,
    paraIndex: 5,
    newContent: "",
  },
  {
    title:
      "Feasibility of near-infrared spectroscopy for online multiple trait assessment of sawn lumber",
    content:
      "Fujimoto T, Kurata Y, Matsumoto K, Tsuchikawa S (2010) Feasibility of near-infrared spectroscopy for online multiple trait assessment of sawn lumber. J Wood Sci 56(6):459",
    disableButton: false,
    paraIndex: 6,
    newContent: "",
  },
  {
    title:
      "Nondestructive estimation of wood chemical composition of sections of radial wood strips by diffuse reflectance near infrared spectroscopy",
    content:
      "Jones PD, Schimleck LR, Peter GF, Daniels RF III (2006) Nondestructive estimation of wood chemical composition of sections of radial wood strips by diffuse reflectance near infrared spectroscopy. Wood Sci Technol 40(8):720",
    disableButton: false,
    paraIndex: 7,
    newContent: "",
  },
  {
    title: "Wood species identification by near-infrared spectroscopy",
    content:
      "Lazarescu C, Hart F, Pirouz Z, Panagiotidis K, Mansfield SD, Barrett JD, Avramidis S (2017) Wood species identification by near-infrared spectroscopy. International Wood Products Journal 8(1):35",
    disableButton: false,
    paraIndex: 8,
    newContent: "",
  },
  {
    title:
      "Near infrared spectroscopic investigation of the thermal degradation of wood",
    content:
      "Mehrotra R, Singh P, Kandpal H (2010) Near infrared spectroscopic investigation of the thermal degradation of wood. Thermochim Acta 507–508:65",
    disableButton: false,
    paraIndex: 9,
    newContent: "",
  },
  {
    title:
      "Nondestructive measurement of fruit and vegetable quality by means of NIR spectroscopy: a review",
    content:
      "Nicolaï BM, Beullens K, Bobelyn E, Peirs A, Saeys W, I.Theron K, Lammertyn J (2007) Nondestructive measurement of fruit and vegetable quality by means of NIR spectroscopy: a review. Postharvest biol technol 46(2):118",
    disableButton: false,
    paraIndex: 10,
    newContent: "",
  },
];
